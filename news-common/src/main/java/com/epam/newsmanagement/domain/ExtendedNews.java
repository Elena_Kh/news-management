package com.epam.newsmanagement.domain;

import java.sql.Timestamp;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alena_Khrapko on 2016-07-25.
 */
public class ExtendedNews {
    private Long id;
    private String title;
    private String shortText;
    private String fullText;
    private Date modificationDate;
    private Timestamp creationDate;
    private Author author;
    private int commentNumber;
    private List<Tag> tagList;
    private List<Comment> commentList;

    public ExtendedNews() {
        this.tagList = new ArrayList<Tag>();
    }

    public ExtendedNews(Long id, String title, String shortText, String fullText, Timestamp creationDate) {
        this.id = id;
        this.title = title;
        this.shortText = shortText;
        this.fullText = fullText;
        this.creationDate = creationDate;
    }

    public ExtendedNews(Long newsId, String title, String shortText, Timestamp creationDate, int commentNumber) {
        this.id = newsId;
        this.title = title;
        this.shortText = shortText;
        this.creationDate = creationDate;
        this.commentNumber = commentNumber;
    }

    public ExtendedNews(News news){
        this.id = news.getId();
        this.title = news.getTitle();
        this.shortText = news.getShortText();
        this.fullText = news.getFullText();
        this.creationDate = news.getCreationDate();
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public void setCommentNumber(int commentNumber) {
        this.commentNumber = commentNumber;
    }

    public void setTagList(List<Tag> tagList) {
        this.tagList = tagList;
    }

    public void setCommentList(List<Comment> commentList) {
        this.commentList = commentList;
    }

    public Long getId() {
        return id;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public String getShortText() {
        return shortText;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public Author getAuthor() {
        return author;
    }

    public int getCommentNumber() {
        return commentNumber;
    }

    public List<Tag> getTagList() {
        return tagList;
    }

    public List<Comment> getCommentList() {
        return commentList;
    }

    public String getFullText() {
        return fullText;
    }
}
