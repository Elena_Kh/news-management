package com.epam.newsmanagement.domain;

import java.sql.Timestamp;

/**
 * Created by Alena_Khrapko on 6/21/2016.
 */
public class Author{
    private Long id;
    private String authorName;
    private Timestamp expired;

    public Author() {
    }

    public Author(Long id, String authorName, Timestamp expired) {
        this.id = id;
        this.authorName = authorName;
        this.expired = expired;
    }

    public Author(Long id, String authorName) {
        this.id = id;
        this.authorName = authorName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public Timestamp getExpired() {
        return expired;
    }

    public void setExpired(Timestamp expired) {
        this.expired = expired;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Author)) return false;

        Author author = (Author) o;

        if (!getId().equals(author.getId())) return false;
        if (!getAuthorName().equals(author.getAuthorName())) return false;
        return getExpired() != null ? getExpired().equals(author.getExpired()) : author.getExpired() == null;

    }

    @Override
    public int hashCode() {
        int result = getId().hashCode();
        result = 31 * result + getAuthorName().hashCode();
        result = 31 * result + (getExpired() != null ? getExpired().hashCode() : 0);
        return result;
    }
}
