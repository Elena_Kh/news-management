package com.epam.newsmanagement.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alena_Khrapko on 6/28/2016.
 */
public class SearchCriteria {
    private Long authorId;
    private List<Long> tagIdList;

    public SearchCriteria() {
        this.authorId = 0L;
        this.tagIdList = new ArrayList<Long>();
    }

    public SearchCriteria(Long authorId, List<Long> tagIdList) {
        this.authorId = authorId;
        this.tagIdList = tagIdList;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public void setTagIdList(List<Long> tagIdList) {
        this.tagIdList = tagIdList;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public List<Long> getTagIdList() {
        return tagIdList;
    }
}
