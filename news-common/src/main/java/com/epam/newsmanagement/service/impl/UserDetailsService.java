package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.domain.Role;
import com.epam.newsmanagement.domain.User;
import com.epam.newsmanagement.service.ServiceException;
import com.epam.newsmanagement.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.SynchronousQueue;

/**
 * Created by Alena_Khrapko on 9/6/2016.
 */
@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        UserDetails userDetails;
        try {
            User user = userService.findByLogin(s);
            if (user != null) {
                List<GrantedAuthority> authorities = new ArrayList<>();
                ///implement method, that select all roles by login
                Role r = userService.selectRole(user.getId());
                System.out.println(r.getRoleName());
                authorities.add(new SimpleGrantedAuthority(r.getRoleName()));
                userDetails = new org.springframework.security.core.userdetails.User(s, user.getPassword(), authorities);
            }else {
                throw new UsernameNotFoundException("Failed to found user with login = " + s);
            }
        } catch (ServiceException e) {
            throw  new UsernameNotFoundException("Failed to found user with login = " + s);
        }
        return userDetails;
    }
}
