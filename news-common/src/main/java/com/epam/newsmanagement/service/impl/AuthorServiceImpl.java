package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.dao.DAOException;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Alena_Khrapko on 6/27/2016.
 */
@Service
public class AuthorServiceImpl implements AuthorService {

    private static Logger logger = LogManager.getLogger(AuthorServiceImpl.class);

    @Autowired
    private AuthorDAO authorDAO;

    @Override
    public List<Author> selectAll() throws ServiceException {
        try {
            return authorDAO.selectAll();
        } catch (DAOException e) {
            logger.error("AuthorService selectAll: " + e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Author> selectAllNotExpired() throws ServiceException {
        try {
            return authorDAO.selectAllNotExpired();
        } catch (DAOException e) {
            logger.error("AuthorService selectAllNotExpired: " + e);
            throw new ServiceException(e);
        }
    }

    @Override
    public Author select(Long id) throws ServiceException {
        try {
            return authorDAO.select(id);
        } catch (DAOException e) {
            logger.error("AuthorService select: " + e);
            throw new ServiceException(e);
        }
    }

    /**
     * If entity has an Id, then this entity already exists and need to be updated.
     * Otherwise the entity need to be created.
     * @param entity
     * @return
     * @throws ServiceException
     */
    @Override
    public Long save(Author entity) throws ServiceException {
        try{
            if (entity.getId() != null){
                authorDAO.update(entity);
                return entity.getId();
            }
            return authorDAO.create(entity);
        } catch (DAOException e){
            logger.error("AuthorService save: " + e);
            throw new ServiceException(e);
        }

    }

    @Override
    public Author selectByNews(Long newsId) throws ServiceException {
        try {
            return authorDAO.selectByNews(newsId);
        } catch (DAOException e) {
            logger.error("AuthorService selectByNews: " + e);
            throw new ServiceException(e);
        }
    }


    @Override
    public boolean delete(Long id) throws ServiceException {
        try {
            return authorDAO.delete(id);
        } catch (DAOException e) {
            logger.error("AuthorService delete: " + e);
            throw new ServiceException(e);
        }
    }
}
