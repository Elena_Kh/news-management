package com.epam.newsmanagement.service;


import com.epam.newsmanagement.domain.*;

import java.util.List;

/**
 * Created by Alena_Khrapko on 6/27/2016.
 */
public interface CombinedService {

    Long create (ExtendedNews news) throws ServiceException;
    Long update (ExtendedNews news) throws ServiceException;
    Long save (ExtendedNews news) throws ServiceException;
    boolean delete (Long newsId) throws ServiceException;
    List<ExtendedNews> selectAll() throws ServiceException;
    List<ExtendedNews> selectAllPerPage(Integer begin, Integer end) throws ServiceException;
    List<ExtendedNews> selectFilteredPerPage(Integer begin, Integer end, SearchCriteria searchCriteria) throws ServiceException;
    int countFilteredNews(SearchCriteria searchCriteria) throws ServiceException;
    ExtendedNews select(Long id) throws ServiceException;
}
