package com.epam.newsmanagement.service;

import com.epam.newsmanagement.domain.Role;
import com.epam.newsmanagement.domain.User;

/**
 * Created by Alena_Khrapko on 6/28/2016.
 */
public interface UserService extends Service<User> {
    User findByLogin(String login) throws ServiceException;
    Role selectRole(Long userId) throws ServiceException;
}
