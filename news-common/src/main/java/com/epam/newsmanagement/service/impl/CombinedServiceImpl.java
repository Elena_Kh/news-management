package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.*;
import com.epam.newsmanagement.dao.impl.ExtendedNewsDAOImpl;
import com.epam.newsmanagement.domain.*;
import com.epam.newsmanagement.service.CombinedService;
import com.epam.newsmanagement.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Alena_Khrapko on 6/28/2016.
 */
@Service
public class CombinedServiceImpl implements CombinedService {

    private static Logger logger = LogManager.getLogger(CombinedServiceImpl.class);

    @Autowired
    private NewsDAO newsDAO;

    @Autowired
    private AuthorDAO authorDAO;

    @Autowired
    private TagDAO tagDAO;

    @Autowired
    private CommentDAO commentDAO;

    @Autowired
    private ExtendedNewsDAOImpl extendedNewsDAO;

    @Override
    @Transactional(rollbackFor = ServiceException.class)
    public Long create(ExtendedNews news) throws ServiceException {
        Long newsId;
        try {
            newsId = newsDAO.create(new News(news.getId(), news.getTitle(), news.getShortText(), news.getFullText(), news.getCreationDate(), news.getModificationDate()));
            if (news.getAuthor() != null){
                newsDAO.insertNewsAuthor(newsId, news.getAuthor().getId());
            }
            if (news.getTagList() != null){
                for(Tag tag : news.getTagList()){
                    newsDAO.insertNewsTag(newsId, tag.getId());
                }
            }
        } catch (DAOException e) {
            logger.error("CombinedService create: " + e);
            throw new ServiceException(e);
        }
        return newsId;
    }

    @Override
    @Transactional(rollbackFor = ServiceException.class)
    public Long update(ExtendedNews news) throws ServiceException {
        try {
            newsDAO.update(new News(news.getId(), news.getTitle(),
                    news.getShortText(), news.getFullText(), news.getCreationDate(),
                    news.getModificationDate()));
            if (news.getAuthor() != null){
                newsDAO.deleteNewsAuthor(news.getId());
                newsDAO.insertNewsAuthor(news.getId(), news.getAuthor().getId());
            }
            if (news.getTagList() != null){
                newsDAO.deleteNewsTag(news.getId());
                for (Tag tag : news.getTagList()){
                    tagDAO.update(tag);
                }
            }
            return news.getId();
        } catch (DAOException e){
            logger.error("CombinedService update: " + e);
            throw new ServiceException(e);
        }
    }

    public Long save (ExtendedNews news) throws ServiceException{
        if (news.getId() == null)
            return create(news);
        return update(news);
    }

    /**
     * First delete news, then delete all its comments
     * @param newsId
     * @return
     * @throws ServiceException
     */
    @Override
    @Transactional(rollbackFor = ServiceException.class)
    public boolean delete(Long newsId) throws ServiceException {
        try {
            if (newsDAO.delete(newsId)){
                return commentDAO.deleteByNewsId(newsId);
            }
        } catch (DAOException e) {
            logger.error("CombinedService delete: " + e);
            throw new ServiceException(e);
        }
        return false;
    }

    @Override
    public List<ExtendedNews> selectAll() throws ServiceException {
        try {
            return extendedNewsDAO.selectAll();
        } catch (DAOException e) {
            logger.error("CombinedService selectAll: " + e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<ExtendedNews> selectAllPerPage(Integer begin, Integer end) throws ServiceException {
        try {
            List<ExtendedNews> newsList = extendedNewsDAO.selectAllPerPage(begin, end);
            List<Tag> tmpTagList;
            for (ExtendedNews news: newsList) {
                tmpTagList = tagDAO.selectByNewsId(news.getId());
                news.setTagList(tmpTagList);
            }
            return newsList;
        } catch (DAOException e) {
            logger.error("CombinedService selectAllPerPage: " + e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<ExtendedNews> selectFilteredPerPage(Integer begin, Integer end,
                                                    SearchCriteria searchCriteria) throws ServiceException {
        try {
            List<ExtendedNews> newsList = extendedNewsDAO.selectFilteredNewsPerPage(begin, end, searchCriteria);
            List<Tag> tmpTagList;
            for(ExtendedNews news: newsList){
                tmpTagList = tagDAO.selectByNewsId(news.getId());
                news.setTagList(tmpTagList);
            }
            return newsList;
        } catch (DAOException e) {
            logger.error("CombinedService selectFilteredPerPage: " + e);
            throw new ServiceException(e);
        }
    }

    @Override
    public int countFilteredNews(SearchCriteria searchCriteria) throws ServiceException {
        try {
            return extendedNewsDAO.countFilteredNews(searchCriteria);
        } catch (DAOException e) {
            logger.error("CombinedService countFilteredNews: " + e);
            throw new ServiceException(e);
        }
    }

    @Override
    public ExtendedNews select(Long id) throws ServiceException {
        try {
            ExtendedNews news = new ExtendedNews(newsDAO.select(id));
            news.setCommentList(commentDAO.selectAll(id));
            Author author = authorDAO.selectByNews(id);
            if (author != null) {
                news.setAuthor(author);
            }
            news.setTagList(tagDAO.selectByNewsId(id));
            return news;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
