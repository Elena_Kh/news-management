package com.epam.newsmanagement.service;


import com.epam.newsmanagement.domain.Comment;

import java.util.List;

/**
 * Created by Alena_Khrapko on 6/27/2016.
 */
public interface CommentService extends Service<Comment> {

    int count(Long newsId) throws ServiceException;
    List<Comment> selectAll(Long newsId) throws ServiceException;
}
