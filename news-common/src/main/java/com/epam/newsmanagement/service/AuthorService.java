package com.epam.newsmanagement.service;


import com.epam.newsmanagement.domain.Author;

import java.util.List;

/**
 * Created by Alena_Khrapko on 6/27/2016.
 */
public interface AuthorService extends Service<Author>{

    Author selectByNews(Long newsId) throws ServiceException;
    List<Author> selectAllNotExpired() throws ServiceException;
}
