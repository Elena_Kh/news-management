package com.epam.newsmanagement.util;

import com.epam.newsmanagement.domain.SearchCriteria;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alena_Khrapko on 6/28/2016.
 */
public class SearchBuilder {

    private static String strStart = "select * from (select b.*, rownum rnum from " +
            "(select distinct news.news_id, news.TITLE, news.SHORT_TEXT, news.CREATION_DATE, news.MODIFICATION_DATE, " +
            "author.AUTHOR_ID, author.author_name, comment_num from news left join " +
            "(select nid, comment_num from " +
            "(select news.news_id as nid, count (*) as comment_num from news " +
            "right join comments on news.news_id = comments.news_id " +
            "group by news.news_id))a on news.NEWS_ID = a.nid " +
            "left join news_author on news.NEWS_ID = news_author.NEWS_ID " +
            "left join author on news_author.AUTHOR_ID = author.AUTHOR_ID " +
            "left join news_tag on news.news_id = news_tag.news_id " +
            "left join tag on news_tag.tag_id = tag.tag_id ";
    private static String strEnd = "order by nvl(comment_num, 0) desc, news.MODIFICATION_DATE) b where rownum <= ?) where rnum >= ?";
    private static String strCountStart = "select count (distinct news.news_id) as quantity from news " +
            "left join news_author on news.news_id = news_author.news_id " +
            "left join news_tag on news.news_id = news_tag.news_id ";

    private static String str1 = "select distinct news.news_id, title, short_text, full_text, creation_date, modification_date from news ";
    private static String str2 = "left join news_author on news.news_id = news_author.news_id ";
    private static String str3 = "left join news_tag on news.news_id = news_tag.news_id ";
    private static String str4 = "where ";
    private static String str5 = "news_author.author_id = ";
    private static String str6 = "and ";
    private static String str7 = "news_tag.tag_id in (";
    private static String str8 = ") ";
    private static String str9 = "order by news.news_id asc";

    public static String buildQuery(SearchCriteria searchCriteria){
        StringBuilder stringBuilder = new StringBuilder(strStart);
        if ((searchCriteria.getAuthorId() != null && searchCriteria.getAuthorId() != 0)
                || !searchCriteria.getTagIdList().isEmpty()){
            stringBuilder.append(str4);
        }
        if (searchCriteria.getAuthorId() != null && searchCriteria.getAuthorId() != 0){
            stringBuilder.append(str5+searchCriteria.getAuthorId()+" ");
        }
        if (searchCriteria.getAuthorId() != null && searchCriteria.getAuthorId() != 0
                && !searchCriteria.getTagIdList().isEmpty()){
            stringBuilder.append(str6);
        }
        if (!searchCriteria.getTagIdList().isEmpty()){
            stringBuilder.append(str7);
            for (int i=0; i<searchCriteria.getTagIdList().size()-1; i++) {
                stringBuilder.append(searchCriteria.getTagIdList().get(i) + ", ");
            }
            stringBuilder.append(searchCriteria.getTagIdList().get(searchCriteria.getTagIdList().size()-1) + str8);
        }
        stringBuilder.append(strEnd);
        /*List<Long> currentList = notNullList(searchCriteria.getTagIdList());
        if (currentList != null && !currentList.isEmpty()){
            stringBuilder.append(str3);
        }
        stringBuilder.append(str4);
        if (searchCriteria.getAuthorId() != null){
            stringBuilder.append(str5);
            stringBuilder.append(searchCriteria.getAuthorId().toString());
        }
        if (searchCriteria.getAuthorId() != null && currentList != null && !currentList.isEmpty()){
            stringBuilder.append(str6);
        }
        if (currentList != null && !currentList.isEmpty()){
            stringBuilder.append(str7);
            for (int i=0; i<currentList.size()-1; i++){
                stringBuilder.append(currentList.get(i).toString() + ", ");
            }
            stringBuilder.append(currentList.get(currentList.size()-1) + str8);
        }
        stringBuilder.append(str9);*/
        return stringBuilder.toString();
    }

    public static String buildCountQuery(SearchCriteria searchCriteria){
        StringBuilder stringBuilder = new StringBuilder(strCountStart);
        if ((searchCriteria.getAuthorId() != null && searchCriteria.getAuthorId() != 0)
                || !searchCriteria.getTagIdList().isEmpty()){
            stringBuilder.append(str4);
        }
        if (searchCriteria.getAuthorId() != null && searchCriteria.getAuthorId() != 0){
            stringBuilder.append(str5+searchCriteria.getAuthorId()+" ");
        }
        if (searchCriteria.getAuthorId() != null && searchCriteria.getAuthorId() != 0
                && !searchCriteria.getTagIdList().isEmpty()){
            stringBuilder.append(str6);
        }
        if (!searchCriteria.getTagIdList().isEmpty()){
            stringBuilder.append(str7);
            for (int i=0; i<searchCriteria.getTagIdList().size()-1; i++) {
                stringBuilder.append(searchCriteria.getTagIdList().get(i) + ", ");
            }
            stringBuilder.append(searchCriteria.getTagIdList().get(searchCriteria.getTagIdList().size()-1) + str8);
        }
        return stringBuilder.toString();
    }

    private static List<Long> notNullList(List<Long> tags){
        if (tags == null || tags.isEmpty()){
            return null;
        }
        List<Long> res = new ArrayList<>();
        for (Long tagId : tags){
            if (tagId != null){
                res.add(tagId);
            }
        }
        return res;
    }
}
