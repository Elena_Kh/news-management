package com.epam.newsmanagement.dao.impl;

import com.epam.newsmanagement.dao.DAOException;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.ExtendedNews;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.SearchCriteria;
import com.epam.newsmanagement.util.SearchBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alena_Khrapko on 2016-07-25.
 */
@Repository
public class ExtendedNewsDAOImpl {

    private static final String SQL_SELECT_ALL =
            "select distinct news.news_id, news.TITLE, news.SHORT_TEXT, news.CREATION_DATE, news.MODIFICATION_DATE, " +
                    "author.author_name, comment_num from news left join " +
                    "(select nid, comment_num from " +
                    "(select news.news_id as nid,  count (*) as comment_num from news " +
                    "right join comments on news.news_id = comments.news_id " +
                    "group by news.news_id))a on news.NEWS_ID = a.nid " +
                    "left join news_author on news.NEWS_ID = news_author.NEWS_ID " +
                    "left join author on news_author.AUTHOR_ID = author.AUTHOR_ID " +
                    "order by nvl(comment_num, 0) desc, news.MODIFICATION_DATE";

    private static final String SQL_SELECT_PER_PAGE =
            "select * from (select b.*, rownum rnum from " +
                    "(select news.news_id, news.TITLE, news.SHORT_TEXT, news.CREATION_DATE, news.MODIFICATION_DATE, " +
                    "author.AUTHOR_ID, author.author_name, comment_num from news left join " +
                    "(select nid, comment_num from " +
                    "(select news.news_id as nid, count (*) as comment_num from news " +
                    "right join comments on news.news_id = comments.news_id " +
                    "group by news.news_id))a on news.NEWS_ID = a.nid " +
                    "left join news_author on news.NEWS_ID = news_author.NEWS_ID " +
                    "left join author on news_author.AUTHOR_ID = author.AUTHOR_ID " +
                    "order by nvl(comment_num, 0) desc, news.MODIFICATION_DATE) b where rownum <= ?) where rnum >= ?";
    @Autowired
    private DataSource dataSource;

    public List<ExtendedNews> selectAll() throws DAOException {
        List<ExtendedNews> newsList = new ArrayList<ExtendedNews>();
        ExtendedNews news = null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_SELECT_ALL);
            ResultSet rs = pr.executeQuery();
            while (rs.next()){
                news = new ExtendedNews(rs.getLong("news_id"), rs.getString("title"), rs.getString("short_text"),
                        rs.getTimestamp("creation_date"), rs.getInt("comment_num"));
                news.setAuthor(new Author(rs.getLong("author_id"), rs.getString("author_name")));
                newsList.add(news);
            }
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return newsList;
    }

    public List<ExtendedNews> selectAllPerPage(Integer begin, Integer end) throws DAOException{
        List<ExtendedNews> newsList = new ArrayList<ExtendedNews>();
        ExtendedNews news;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_SELECT_PER_PAGE);
            pr.setString(2, begin.toString());
            pr.setString(1, end.toString());
            ResultSet rs = pr.executeQuery();
            while (rs.next()){
                news = new ExtendedNews(rs.getLong("news_id"), rs.getString("title"), rs.getString("short_text"),
                        rs.getTimestamp("creation_date"), rs.getInt("comment_num"));
                news.setAuthor(new Author(rs.getLong("author_id"), rs.getString("author_name")));
                newsList.add(news);
            }
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return newsList;
    }

    public List<ExtendedNews> selectFilteredNewsPerPage(Integer begin, Integer end,
                                                        SearchCriteria searchCriteria) throws DAOException {
        List<ExtendedNews> newsList = new ArrayList<ExtendedNews>();
        ExtendedNews news;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SearchBuilder.buildQuery(searchCriteria));
            pr.setString(2, begin.toString());
            pr.setString(1, end.toString());
            ResultSet rs = pr.executeQuery();
            while (rs.next()){
                news = new ExtendedNews(rs.getLong("news_id"), rs.getString("title"), rs.getString("short_text"),
                        rs.getTimestamp("creation_date"), rs.getInt("comment_num"));
                news.setAuthor(new Author(rs.getLong("author_id"), rs.getString("author_name")));
                newsList.add(news);
            }
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return newsList;
    }

    public int countFilteredNews(SearchCriteria searchCriteria) throws DAOException {
        int num;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SearchBuilder.buildCountQuery(searchCriteria));
            ResultSet resultSet = pr.executeQuery();
            resultSet.next();
            num = resultSet.getInt("quantity");
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return num;
    }
}
