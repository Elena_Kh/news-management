package com.epam.newsmanagement.dao.impl;

import com.epam.newsmanagement.dao.DAOException;
import com.epam.newsmanagement.dao.UserDAO;
import com.epam.newsmanagement.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class UserDAOImpl implements UserDAO {

    private static final String SQL_SELECT_ALL = "select user_id, user_name, login, password from users";
    private static final String SQL_SELECT_USER = "select user_name, login, password from users where user_id = ?";
    private static final String SQL_INSERT_USER = "insert into users (user_id, user_name, login, password) values " +
            "(users_seq.nextval, ?, ?, ?)";
    private static final String SQL_DELETE_USER = "delete from users where user_id = ?";
    private static final String SQL_UPDATE_USER = "update users set user_name = ?, login = ?, password = ? " +
            "where user_id = ?";
    private static final String SQL_SELECT_USER_BY_LOGIN =
            "select user_id, user_name, password from users where login = ?";

    @Autowired
    private DataSource dataSource;

    @Override
    public List<User> selectAll() throws DAOException {
        List<User> userList = new ArrayList<>();
        User user = null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_SELECT_ALL);
            ResultSet rs = pr.executeQuery();
            while (rs.next()){
                user = new User(rs.getLong("user_id"), rs.getString("user_name"), rs.getString("login"),
                        rs.getString("password"));
                userList.add(user);
            }
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return userList;
    }

    @Override
    public User select(Long id) throws DAOException {
        User user = null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_SELECT_USER);
            pr.setString(1, Long.toString(id));
            ResultSet rs = pr.executeQuery();
            while (rs.next()){
                user = new User(id, rs.getString("user_name"), rs.getString("login"), rs.getString("password"));
            }
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return user;
    }

    @Override
    public boolean delete(Long id) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_DELETE_USER);
            pr.setString(1, Long.toString(id));
            pr.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return true;
    }

    @Override
    public Long create(User entity) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        String[] generated = {"USER_ID"};
        Long insertedId;
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_INSERT_USER);
            setStatment(pr, entity.getUserName(), entity.getLogin(), entity.getPassword());
            pr.executeUpdate();
            ResultSet generatedKeys = pr.getGeneratedKeys();
            if (generatedKeys.next()) {
                insertedId = generatedKeys.getLong(1);
            } else {
                throw new DAOException("No ID obtained");
            }
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return insertedId;
    }

    @Override
    public boolean update(User entity) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_UPDATE_USER);
            setStatment(pr, entity.getUserName(), entity.getLogin(), entity.getPassword(), entity.getId());
            pr.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return true;
    }

    @Override
    public User findByLogin(String login) throws DAOException {
        User user = null;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try {
            PreparedStatement pr = connection.prepareStatement(SQL_SELECT_USER_BY_LOGIN);
            pr.setString(1, login);
            ResultSet rs = pr.executeQuery();
            while (rs.next()){
                user = new User(rs.getLong("user_id"), rs.getString("user_name"), login, rs.getString("password"));
            }
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return user;
    }
}
