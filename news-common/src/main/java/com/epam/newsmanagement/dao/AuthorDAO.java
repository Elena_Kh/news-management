package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.domain.Author;

import java.util.List;

public interface AuthorDAO extends DAO<Author> {
    Author selectByNews (Long newsId) throws DAOException;
    List<Author> selectAllNotExpired() throws DAOException;
}
