package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.domain.Tag;

import java.util.List;

public interface TagDAO extends DAO<Tag> {

    List<Tag> selectByNewsId(Long newsId) throws DAOException;
    boolean deleteNewsTag(Long tagId) throws DAOException;
}
