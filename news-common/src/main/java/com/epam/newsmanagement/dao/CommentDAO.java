package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.domain.Comment;

import java.util.List;

public interface CommentDAO extends DAO<Comment> {

    int count(Long newsId) throws DAOException;
    List<Comment> selectAll(Long newsId) throws DAOException;
    boolean deleteByNewsId(Long newsId) throws DAOException;
}
