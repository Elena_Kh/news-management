package com.epam.newsmanagement.dao;


import com.epam.newsmanagement.domain.User;

public interface UserDAO extends DAO<User> {
    User findByLogin(String login) throws DAOException;
}
