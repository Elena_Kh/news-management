package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.domain.Role;

public interface RoleDAO extends DAO<Role> {
}
