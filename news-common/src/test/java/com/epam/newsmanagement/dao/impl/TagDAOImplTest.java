package com.epam.newsmanagement.dao.impl;

import com.epam.newsmanagement.dao.DAOException;
import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.domain.Tag;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.unitils.UnitilsJUnit4;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Alena_Khrapko on 6/29/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:test-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
        DbUnitTestExecutionListener.class,
        TransactionDbUnitTestExecutionListener.class })
@DatabaseSetup("classpath:dataset/tagDAOTest.xml")
@DatabaseTearDown(value = { "classpath:dataset/tagDAOTest.xml" }, type = DatabaseOperation.DELETE_ALL)
public class TagDAOImplTest extends UnitilsJUnit4 {

    @Autowired
    private TagDAO tagDAO;

    @Test
    public void testCreate() throws DAOException {
        Tag tag = new Tag(null, "new Tag");
        Long actual = tagDAO.create(tag);
        assertNotNull(actual);
        assertTrue(actual > 0);
    }

    @Test
    public void testUpdate() throws DAOException {
        Tag tag = new Tag(1L, "new Tag");
        Boolean actual = tagDAO.update(tag);
        assertTrue(actual);
    }

    @Test
    public void testDelete() throws DAOException {
        Boolean actual = tagDAO.delete(1L);
        assertTrue(actual);
    }

    @Test
    public void testSelectById() throws DAOException {
        Tag expected = new Tag(1L, "Tag 1");
        Tag actual = tagDAO.select(1L);
        assertEquals(expected, actual);
    }

    @Test
    public void testSelectAll() throws DAOException {
        List<Tag> expected = new ArrayList<>();
        expected.add(new Tag(1L, "Tag 1"));
        expected.add(new Tag(2L, "Tag 2"));
        expected.add(new Tag(3L, "Tag 3"));
        List<Tag> actual = tagDAO.selectAll();
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void selectAllByNewsId() throws DAOException {
        Long newsId = 1L;
        List<Tag> expected = new ArrayList<>();
        expected.add(new Tag(2L, "Tag 2"));
        expected.add(new Tag(3L, "Tag 3"));
        List<Tag> actual = tagDAO.selectByNewsId(newsId);
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void deleteNewsTag() throws DAOException {
        assertTrue(tagDAO.deleteNewsTag(1L));
    }
}
