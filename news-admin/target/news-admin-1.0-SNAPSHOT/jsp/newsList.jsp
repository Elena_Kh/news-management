<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<form:form action="searchNews" method="post" modelAttribute="searchCriteria">
    <form:select id="author-select" path="authorId">
        <option value="0">Please select the author</option>
        <c:forEach var="authors" items="${authorList}">
            <c:choose>
                <c:when test="${authors.id eq searchCriteria.authorId}">
                    <option selected value="${authors.id}">${authors.authorName}</option>
                </c:when>
                <c:otherwise>
                    <option value="${authors.id}">${authors.authorName}</option>
                </c:otherwise>
            </c:choose>

        </c:forEach>
    </form:select>

    <select id="tag-select" name="tagId" multiple="multiple">
        <c:forEach var="tag" items="${tagList}">
            <c:set var="flag" value="false"></c:set>
            <c:forEach var="tag2" items="${searchCriteria.tagIdList}">
                <c:if test="${tag.id eq tag2}">
                    <c:set var="flag" value="true"></c:set>
                </c:if>
            </c:forEach>
            <c:choose>
                <c:when test="${flag}">
                    <option selected value="${tag.id}">${tag.tagName}</option>
                </c:when>
                <c:otherwise>
                    <option value="${tag.id}">${tag.tagName}</option>
                </c:otherwise>
            </c:choose>
        </c:forEach>
    </select>

    <button type="submit" value="filter">Filter</button>
    <button name="reset" type="submit" value="reset">Reset</button>
<%--</form:form>--%>
    <br/>

<c:set var="news" value="com.epam.newsmanagement.domain.ExtendedNews"/>
<c:choose>
    <c:when test="${empty newsList}">
        <c:out value="No news were found!"></c:out>
    </c:when>
    <c:otherwise>
        <c:forEach var="news" items="${newsList}">
            <a id="blue-link" href="news?id=${news.id}"><c:out value="${news.title}"/></a>
            <c:if test="${not empty news.author.authorName}">
                <c:out value="(by ${news.author.authorName})"/>
            </c:if>
            <fmt:formatDate pattern="dd/MM/yyyy" value="${news.creationDate}"/><br/>
            <c:out value="${news.shortText}"/><br/>
            <c:set var="tag" value="com.epam.newsmanagement.domain.Tag"/>
            <c:forEach var="tag" items="${news.tagList}">
                <c:out value="${tag.tagName}"/>
            </c:forEach>
            <c:out value="Comments(${news.commentNumber})"/>
            <a href="addUpdateNews?id=${news.id}" class="btn btn-primary">Edit</a>
            <br/><br/>
        </c:forEach>
    </c:otherwise>
</c:choose>

<%--<form:form action="newsList" method="get">--%>
    <c:if test="${buttonNum > 1}">
        <c:forEach var="i" begin="1" end="${buttonNum}">
            <c:choose>
                <c:when test="${i == activeButton}">
                    <button name="button" type="submit" class="active-button" value="${i}">
                        <c:out value="${i}"/>
                    </button>
                </c:when>
                <c:otherwise>
                    <button name="button" type="submit" value="${i}">
                        <c:out value="${i}"/>
                    </button>
                </c:otherwise>
            </c:choose>
        </c:forEach>
    </c:if>
</form:form>