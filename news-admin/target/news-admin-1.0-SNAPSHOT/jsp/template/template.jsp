<%--
  Created by IntelliJ IDEA.
  User: Alena_Khrapko
  Date: 2016-07-19
  Time: 6:03 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" media="all"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery.multiselect.css" media="all"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery-ui.min.css" media="all"/>
    <script src="${pageContext.request.contextPath}/js/jquery-3.1.0.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/jquery-ui.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/jquery.multiselect.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/style.js"></script>
    <title>News-admin</title>
</head>
<body link="black" vlink="black">

<c:set var="user" value="<sec:authentication property='principal.username'/>"/>

<div class="container">
    <!-- Header -->
    <div style="border: black solid 2px">
        <tiles:insertAttribute name="header" />
    </div>

    <!-- Menu Page -->
    <c:if test="${not empty user}">
        <div class="span-5  border" style="height:200px;background-color:#FCFCFC;">
            <tiles:insertAttribute name="menu" />
        </div>
    </c:if>

    <!-- Body Page -->
    <div class="span-19 last">
        <tiles:insertAttribute name="body" />
    </div>
    <!-- Footer Page -->
    <div style="text-align: center; border: black solid 2px; margin: 0">
        <tiles:insertAttribute name="footer" />
    </div>
</div>
</body>
</html>