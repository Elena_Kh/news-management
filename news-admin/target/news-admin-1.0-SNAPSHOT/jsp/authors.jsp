<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="author" value="com.epam.newsmanagement.domain.Author"/>
<c:forEach var="author" items="${authorList}">
    <c:choose>
        <c:when test="${not empty selectedId && author.id == selectedId}">
            <form:form action="update" method="post" modelAttribute="author">
                <c:out value="Author:"/>
                <form:input path="id" value="${author.id}" type="hidden"/>
                <form:input path="authorName" value="${author.authorName}"/>
                <input name="update" class="link-button" type="submit" value="update"/>
                <c:if test="${empty author.expired}">
                    <input name="expire" class="link-button" type="submit" value="expired"/>
                </c:if>
                <input name="cancel" class="link-button" type="submit" value="cancel">
            </form:form>
        </c:when>
        <c:otherwise>
            <c:out value="Author:"/>
            <input type="text" value="${author.authorName}" disabled>
            <a href="/admin/edit?selectedId=${author.id}" class="btn btn-primary">edit</a>
            <br/>
        </c:otherwise>
    </c:choose>
    <br/>
</c:forEach>
<br/>
<form:form action="addAuthor" method="post" modelAttribute="author" id="addForm">
    <c:out value="Add author:"/>
    <form:input path="authorName" id="authorName"></form:input>
    <input class="link-button" type="submit" value="save">
</form:form>
