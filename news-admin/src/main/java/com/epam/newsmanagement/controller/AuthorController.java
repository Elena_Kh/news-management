package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by Alena_Khrapko on 2016-07-21.
 */
@Controller
public class AuthorController {

    @Autowired
    AuthorService authorService;

    @RequestMapping(value = "/authors")
    public String viewAuthors(Model model) throws ServiceException {
        List<Author> authorList = authorService.selectAll();
        model.addAttribute("authorList", authorList);
        model.addAttribute("author", new Author());
        return "authors";
    }

    @RequestMapping(value = "/addAuthor", method = RequestMethod.POST)
    public String addAuthor(@ModelAttribute("author")Author author) throws ServiceException {
        authorService.save(author);
        return "redirect:/authors";
    }

    @RequestMapping(value = "/edit")
    public String enableEdit(@RequestParam("selectedId") Long id, Model model) throws ServiceException {
        model.addAttribute("selectedId", id);
        return viewAuthors(model);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST, params = "update")
    public String updateAuthor(@ModelAttribute("author") Author author) throws ServiceException {
        Author prevAuthor = authorService.select(author.getId());
        if (prevAuthor.getExpired() != null){
            author.setExpired(prevAuthor.getExpired());
        }
        authorService.save(author);
        return "redirect:/authors";
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST, params = "expire")
    public String expiredAuthor(@ModelAttribute("author") Author author) throws ServiceException {
        author.setExpired(new Timestamp(System.currentTimeMillis()));
        authorService.save(author);
        return "redirect:/authors";
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST, params = "cancel")
    public String cancelAuthorEdit(@ModelAttribute("author") Author author){
        return "redirect:/authors";
    }
}
