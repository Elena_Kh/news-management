package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.domain.*;
import com.epam.newsmanagement.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by Alena_Khrapko on 2016-07-19.
 */
@Controller
public class NewsController {

    private final Integer NUMBER_NEWS_PER_PAGE = 5;

    @Autowired
    NewsService newsService;

    @Autowired
    CombinedService combinedService;

    @Autowired
    CommentService commentService;

    @Autowired
    AuthorService authorService;

    @Autowired
    TagService tagService;

    @RequestMapping(value="/news")
    public String viewNews(@RequestParam("id") Long newsId, Model model) throws ServiceException {
        ExtendedNews news = combinedService.select(newsId);
        model.addAttribute("news", news);
        model.addAttribute("comment", new Comment());
        return "news";
    }

    @RequestMapping(value = "/newsList")
    public ModelAndView allNews(/*@RequestParam(value = "button", required = false) Integer buttonId*/) throws ServiceException {
        ModelAndView model = new ModelAndView();
        model.setViewName("newsList");
        Integer countNews = newsService.count();
        Integer buttonNum = countNews/NUMBER_NEWS_PER_PAGE;
        if (countNews%NUMBER_NEWS_PER_PAGE != 0){
            buttonNum++;
        }
        /*if (buttonId == null){
            buttonId = 1;
        }*/ Integer buttonId = 1;
        model.addObject("buttonNum", buttonNum);
        model.addObject("activeButton", buttonId);
        List<ExtendedNews> newsList = combinedService.selectAllPerPage((buttonId-1)*NUMBER_NEWS_PER_PAGE+1,Math.min(countNews, buttonId*NUMBER_NEWS_PER_PAGE));
        model.addObject("newsList", newsList);
        model.addObject("authorList", authorService.selectAll());
        model.addObject("tagList", tagService.selectAll());
        model.addObject("searchCriteria", new SearchCriteria());
        return model;
    }

    @RequestMapping(value = "/addComment", method = RequestMethod.POST)
    public String addComment(@ModelAttribute("comment") Comment comment) throws ServiceException{
        comment.setCreationDate(new Timestamp(System.currentTimeMillis()));
        commentService.save(comment);
        return "redirect:/news?id=" + comment.getNewsId();
    }

    @RequestMapping(value = "/addUpdateNews")
    public ModelAndView addNews(@RequestParam(value = "id", required = false) Long newsId) throws ServiceException {
        ModelAndView model = new ModelAndView();
        if (newsId != null){
            model.addObject("news", combinedService.select(newsId));
        } else {
            ExtendedNews news = new ExtendedNews();
            news.setCreationDate(new Timestamp(System.currentTimeMillis()));
            model.addObject("news", news);
        }
        List<Author> authorList = authorService.selectAllNotExpired();
        model.addObject("authorList", authorList);
        List<Tag> tagList = tagService.selectAll();
        model.addObject("tagList", tagList);
        model.setViewName("addNews");
        return model;
    }

    @RequestMapping(value = "/addExtendedNews", method = RequestMethod.POST)
    public String addExtendedNews(@ModelAttribute("news") ExtendedNews news, @RequestParam(value = "tagId", required = false) Long... tagId) throws ServiceException {
        if (news.getId() == null) {
            news.setCreationDate(new Timestamp(System.currentTimeMillis()));
        }
        news.setModificationDate(new Date(System.currentTimeMillis()));
        for (Long id: tagId) {
            Tag tag = new Tag();
            tag.setId(id);
            news.getTagList().add(tag);
        }
        Long newsId = combinedService.save(news);
        return "redirect:/news?id=" + newsId;
    }

    @RequestMapping(value = "/searchNews", method = RequestMethod.POST)
    public ModelAndView filterViewNews(@RequestParam(value = "button", required = false) Integer buttonId,
                                       @ModelAttribute("searchCriteria") SearchCriteria searchCriteria,
                                       @RequestParam(value = "tagId", required = false) Long... tagId) throws ServiceException {


        if (tagId != null) {
            for (Long id : tagId) {
                searchCriteria.getTagIdList().add(id);
            }
        }
        ModelAndView model = new ModelAndView();
        model.setViewName("newsList");
        Integer countNews = combinedService.countFilteredNews(searchCriteria);
        Integer buttonNum = countNews/NUMBER_NEWS_PER_PAGE;
        if (countNews%NUMBER_NEWS_PER_PAGE != 0){
            buttonNum++;
        }
        if (buttonId == null){
            buttonId = 1;
        }
        model.addObject("buttonNum", buttonNum);
        model.addObject("activeButton", buttonId);
        List<ExtendedNews> newsList = combinedService.selectFilteredPerPage(
                (buttonId-1)*NUMBER_NEWS_PER_PAGE+1,
                Math.min(countNews, buttonId*NUMBER_NEWS_PER_PAGE), searchCriteria);
        model.addObject("newsList", newsList);
        model.addObject("authorList", authorService.selectAll());
        model.addObject("tagList", tagService.selectAll());
        model.addObject("searchCriteria", searchCriteria);
        ////delete
        model.addObject("username", "Vasia");
        return model;
    }

    @RequestMapping(value = "/searchNews", method = RequestMethod.POST, params = "reset")
    public String resetFilterNews(){
        return "redirect:/newsList";
    }
}
