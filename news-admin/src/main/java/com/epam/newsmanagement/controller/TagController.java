package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.service.ServiceException;
import com.epam.newsmanagement.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Created by Alena_Khrapko on 2016-07-21.
 */
@Controller
public class TagController {

    @Autowired
    TagService tagService;

    @RequestMapping(value = "/tags")
    public String viewTags(Model model) throws ServiceException {
        List<Tag> tagList = tagService.selectAll();
        model.addAttribute("tagList", tagList);
        model.addAttribute("tag", new Tag());
        return "tags";
    }

    @RequestMapping(value = "/addTag", method = RequestMethod.POST)
    public String addTag(@ModelAttribute("tag") Tag tag) throws ServiceException {
        tagService.save(tag);
        return "redirect:/tags";
    }

    @RequestMapping(value = "/editTag")
    public String enableEdit(@RequestParam("selectedId") Long id, Model model) throws ServiceException {
        model.addAttribute("selectedId", id);
        return viewTags(model);
    }

    @RequestMapping(value = "/updateTag", method = RequestMethod.POST, params = "update")
    public String updateTag(@ModelAttribute("tag") Tag tag) throws ServiceException {
        tagService.save(tag);
        return "redirect:/tags";
    }

    @RequestMapping(value = "/updateTag", method = RequestMethod.POST, params = "delete")
    public String deleteTag(@ModelAttribute("author") Tag tag) throws ServiceException {
        tagService.delete(tag.getId());
        return "redirect:/tags";
    }

    @RequestMapping(value = "/updateTag", method = RequestMethod.POST, params = "cancel")
    public String cancelTagEdit(@ModelAttribute("tag") Tag tag){
        return "redirect:/tags";
    }
}
