package com.epam.newsmanagement.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Alena_Khrapko on 2016-07-21.
 */
@Controller
public class UserController {

    @RequestMapping(value="/index")
    public String index() {
        return "index";
    }

}
