package com.epam.newsmanagement.utils;

import com.epam.newsmanagement.domain.Tag;
import org.springframework.core.convert.converter.Converter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Alena_Khrapko on 8/30/2016.
 */
public class TagConverter implements Converter<String, Tag> {
    @Override
    public Tag convert(String s) {
        Tag tag = new Tag();
        /*String pattern = new String("tagId=\\d+");
        Pattern pt = Pattern.compile(pattern);
        Matcher matcher = pt.matcher(s);
        if (matcher.find()){*/
            tag.setId(Long.parseLong(s));
        /*}*/
        return tag;
    }
}
