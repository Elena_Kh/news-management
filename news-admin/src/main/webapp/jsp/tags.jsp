<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="tag" value="com.epam.newsmanagement.domain.Tag"></c:set>
<c:forEach var="tag" items="${tagList}">
    <c:choose>
        <c:when test="${not empty selectedId && tag.id == selectedId}">
            <form:form action="updateTag" method="post" modelAttribute="tag">
                <c:out value="Tag:"/>
                <form:input path="id" value="${tag.id}" type="hidden"/>
                <form:input path="tagName" value="${tag.tagName}"/>
                <input name="update" class="link-button" type="submit" value="update"/>
                <input name="delete" class="link-button" type="submit" value="delete"/>
                <input name="cancel" class="link-button" type="submit" value="cancel">
            </form:form>
        </c:when>
        <c:otherwise>
            <c:out value="Tag:"/>
            <input type="text" value="${tag.tagName}" disabled>
            <a href="/editTag?selectedId=${tag.id}" class="btn btn-primary">edit</a>
            <br/>
        </c:otherwise>
    </c:choose>
    <br/>
</c:forEach>
<br/>
<form:form action="addTag" method="post" modelAttribute="tag" id="addForm">
    <c:out value="Add Tg:"/>
    <form:input path="tagName" id="tagName"></form:input>
    <input class="link-button" type="submit" value="save">
</form:form>
