<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<h2>${news.title}</h2>
<fmt:formatDate pattern="dd/MM/yyyy" value="${news.creationDate}"/><br/>
<c:if test="${not empty news.author.authorName}">
    <p>(by ${news.author.authorName})</p>
</c:if>
<p>${news.fullText}</p><br/>
<br/>
<c:set var="comment" value="com.epam.newsmanagement.domain.Comment"/>
<c:forEach var="comment" items="${news.commentList}">
    <fmt:formatDate pattern="dd/MM/yyyy" value="${comment.creationDate}"/><br/>
    <c:out value="${comment.commentText}"/>
    <br/><br/>
</c:forEach>
<form:form action="addComment" method="post" modelAttribute="comment" id="addForm">
    <form:textarea path="commentText" rows="4" cols="70" id="commentText"></form:textarea><br/>
    <form:input path="newsId" value="${news.id}" type="hidden" />
    <input type="submit" value="Post comment">
</form:form>
<%--<form:textarea path="addComment" rows="4" cols="70" pattern=".{5,100}" id="commentText"/>--%>
<a href="news?id=${news.id-1}" class="btn btn-primary">PREVIOUS</a>
<a href="news?id=${news.id+1}" class="btn btn-primary">NEXT</a>