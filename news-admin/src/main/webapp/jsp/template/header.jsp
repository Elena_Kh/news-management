<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<c:set var="user" value="<sec:authentication property='principal.username'/>"/>
<div style="right: auto; margin-bottom: 5px">
    <c:if test="${not empty user}">
        <label style="text-align: right">
            Hello, ${user}
            <sec:authentication property='principal.username'/>
        </label>
        <form action="<c:url value='/j_spring_security_logout'/>">
            <input type="submit" value="Logout"/>
        </form>
    </c:if>
</div>

<h1>News portal - Administration</h1>