<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<p>Menu</p>
<ul style="list-style:none;line-height:28px;width: 20%">
    <li>
        <spring:url value="/newsList" var="newsListUrl" htmlEscape="true" />
        <a href="${newsListUrl}">News List</a>
    </li>
    <li>
        <spring:url value="/addUpdateNews" var="addNewsUrl" htmlEscape="true" />
        <a href="${addNewsUrl}">Add News</a>
    </li>
    <li>
        <spring:url value="/authors" var="authorsUrl" htmlEscape="true" />
        <a href="${authorsUrl}">Add/Update Authors</a>
    </li>
    <li>
        <spring:url value="/tags" var="tagsUrl" htmlEscape="true" />
        <a href="${tagsUrl}">Add/Update Tags</a>
    </li>
</ul>