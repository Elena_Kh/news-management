<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--<c:set var="news" value="com.epam.newsmanagement.domain.ExtendedNews"></c:set>--%>
<%--<c:set var="author" value="com.epam.newsmanagement.domain.Author"></c:set>--%>
<form:form action="addExtendedNews" method="post" modelAttribute="news">
    <form:input path="id" hidden="true" value="${news.id}"/>
    <c:out value="Title:"></c:out>
    <form:input path="title" id="title" value="${news.title}" ></form:input><br/><br/>
    <c:out value="Date:"></c:out>
    <input path="creationDate" id="creationDate" required readonly
           value="<fmt:formatDate pattern='dd/MM/yyyy' value='${news.creationDate}'/>"/><br/><br/>
    <c:out value="Brief:"></c:out>
    <form:textarea path="shortText" id="shortText" value="${news.shortText}" required="true"></form:textarea><br/><br/>
    <c:out value="Content:"></c:out>
    <form:textarea path="fullText" id="fullText" value="${news.fullText}" required="true"></form:textarea><br/><br/>

    <form:select path="author.id">
        <form:option value="0" disabled="true">Please select the author</form:option>
        <c:forEach var="authors" items="${authorList}">
            <c:choose>
                <c:when test="${authors eq news.author}">
                    <form:option selected="true" value="${authors.id}">${authors.authorName}</form:option>
                </c:when>
                <c:otherwise>
                    <form:option value="${authors.id}">${authors.authorName}</form:option>
                </c:otherwise>
            </c:choose>
        </c:forEach>
        <%--<form:options items="${authorList}" itemValue="id" itemLabel="authorName"/>--%>
    </form:select>

    <%--<div class="multiselect">
        <div class="selectBox" onclick="showCheckboxes()">
            <select>
                <option>Please select tags</option>
            </select>
            <div class="overSelect"></div>
        </div>
        <div id="checkboxes">
            <c:forEach var="tag" items="${tagList}">
                <label for="${tag.id}">
                    <input type="checkbox" id="${tag.id}"/>
                    <c:out value="${tag.tagName}"></c:out>
                </label>
            </c:forEach>
        </div>
    </div><br/>--%>

    <select id="tag-select" name="tagId" multiple="multiple">
        <c:forEach var="tag" items="${tagList}">
            <c:set var="flag" value="false"></c:set>
            <c:forEach var="tag2" items="${news.tagList}">
                <c:if test="${tag.id eq tag2.id}">
                    <c:set var="flag" value="true"></c:set>
                </c:if>
            </c:forEach>
            <c:choose>
                <c:when test="${flag}">
                    <option selected value="${tag.id}">${tag.tagName}</option>
                </c:when>
                <c:otherwise>
                    <option value="${tag.id}">${tag.tagName}</option>
                </c:otherwise>
            </c:choose>
        </c:forEach>
    </select>

    <input type="submit" value="Save">
</form:form>