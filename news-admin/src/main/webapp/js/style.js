/**
 * Created by Alena_Khrapko on 8/26/2016.
 */
var expanded = false;
function showCheckboxes() {
    var checkboxes = document.getElementById("checkboxes");
    if (!expanded) {
        checkboxes.style.display = "block";
        expanded = true;
    } else {
        checkboxes.style.display = "none";
        expanded = false;
    }
}


$("select").multiselect({
    header: "Choose an Option!"
    /*selectedText: "# of # selected"*/
});

$(document).ready(function(){
    $("#tag-select").multiselect();
});